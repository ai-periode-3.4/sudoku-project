package com.avans.sudoku;

import java.util.HashSet;
import java.util.Set;

public class Number {

    public int x;
    public int y;

    public int value;

    public Set<Integer> possible;

    public Number(int x, int y, int value, Set<Integer> possible) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.possible = possible;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Set<Integer> getPossible() {
        return possible;
    }

    public void remove(int value) {
        possible.remove(value);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        if (!hasValue()) {
            this.value = value;
            this.possible = new HashSet<>();
        }
    }

    public void setPossible(Set<Integer> possible) {
        this.possible = possible;
    }

    public boolean hasValue() {
        return value != 0;
    }

    @Override
    public String toString() {
        return "x:" + x +
                " y:" + y +
                " possible=" + possible + " ";
    }

    @Override
    public Number clone() {
        return new Number(x, y, value, new HashSet<>(possible));
    }
}
