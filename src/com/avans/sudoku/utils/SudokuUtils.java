package com.avans.sudoku.utils;

import com.google.inject.internal.util.Lists;
import de.sfuhrm.sudoku.Riddle;

import java.util.HashSet;
import java.util.Set;

import com.avans.sudoku.Number;


public class SudokuUtils {

    public static Set<Integer> getPossibleSolutions(Riddle riddle, int column, int row) {
        boolean[] possible = new boolean[]{true, true, true, true, true, true, true, true, true};

        int value = riddle.get(column, row);

        if (value != 0)
            return new HashSet<>();

        //get column
        for (int i = 0; i <= 8; i++) {
            int rowValue = riddle.get(column, i);

            if (rowValue != 0)
                possible[rowValue - 1] = false;
        }

        //get row
        for (int i = 0; i <= 8; i++) {
            int rowValue = riddle.get(i, row);

            if (rowValue != 0)
                possible[rowValue - 1] = false;
        }

        //get block
        for (int i : getUnplayableNumbersBlock(column, row, riddle)) {
            possible[i - 1] = false;
        }

        Set<Integer> possibleNumbers = new HashSet<>();

        for (int i = 0; i <= 8; i++) {
            if (possible[i]) {
                possibleNumbers.add(i + 1);
            }
        }

        return possibleNumbers;
    }

    public static Set<Integer> getUnplayableNumbersBlock(int column, int row, Riddle riddle) {
        int blockX = (column >= 0 && column <= 2) ? 0 : (column >= 3 && column <= 5) ? 1 : 2;
        int blockY = (row >= 0 && row <= 2) ? 0 : (row >= 3 && row <= 5) ? 1 : 2;

        Set<Integer> unplayable = new HashSet<>();

        for (int x = (3 * blockX); x <= 2 + (3 * blockX); x++) {
            for (int y = (3 * blockY); y <= 2 + (3 * blockY); y++) {
                int value = riddle.get(x, y);

                if (value != 0)
                    unplayable.add(value);

            }
        }

        return unplayable;
    }

    public static boolean hasValue(Number[][] block, int value) {
        for (Number[] row : block) {
            for (Number n : row) {
                if (n.getValue() == value)
                    return true;
            }
        }

        return false;
    }

    public static int getNumberCountBlock(Number[][] block, int value) {
        if (hasValue(block, value))
            return 0;

        int count = 0;

        for (Number[] row : block) {
            for (Number n : row) {
                if (n.hasValue() && n.getValue() == value) {
                    return 0;
                } else if (n.getPossible().contains(value))
                    count++;
            }
        }

        return count;
    }

    public static int getNumberCountRow(Number[] row, int value) {
        int i = 0;

        for (Number n : row) {
            if (n.hasValue() && n.getValue() == value)
                return 0;

            else if (n.getPossible().contains(value)) {
                i++;
            }
        }

        return i;
    }

    public static Number[][] splice(Number[][] block, int x, int y) {
        Number[][] numbers = new Number[3][3];

        for (int blockX = 0; blockX < 3; blockX++) {
            System.arraycopy(block[blockX + (x * 3)], (y * 3), numbers[blockX], 0, 3);
        }

        return numbers;
    }

    public static void update(Riddle riddle, Number[][] block, int x, int y, int value) {
        block[x][y].setValue(value);
        riddle.set(x, y, (byte) value);


        //row update
        for (int x1 = 0; x1 < 8; x1++) {
            block[x1][y].remove(value);
        }

        //column update
        for (int y1 = 0; y1 < 8; y1++) {
            block[x][y1].remove(value);
        }

        updateBlock(block, x, y, value);

        System.out.println("Found: " + value + " at x: " + x + " y: " + y);
    }

    public static void updateBlock(Number[][] block, int x, int y, int value) {
        int blockX = (x >= 0 && x <= 2) ? 0 : (x >= 3 && x <= 5) ? 1 : 2;
        int blockY = (y >= 0 && y <= 2) ? 0 : (y >= 3 && y <= 5) ? 1 : 2;

        for (int x1 = (3 * blockX); x1 <= 2 + (3 * blockX); x1++) {
            for (int y1 = (3 * blockY); y1 <= 2 + (3 * blockY); y1++) {
                block[x1][y1].remove(value);
            }
        }
    }

    public static boolean isFinished(Number[][] block) {
        for (Number[] numbers : block) {
            for (Number n : numbers) {
                if (!n.hasValue())
                    return false;
            }
        }

        return true;
    }

    public static Number getNumberWithPossibilityBlock(Number[][] block, int value) {
        for (Number[] numbers : block) {
            for (Number n : numbers) {
                if (n.getPossible().contains(value))
                    return n;
            }
        }

        return null;
    }

    public static Number getNumberWithPossibilityRow(Number[] row, int value) {
        for (Number n : row) {
            if (n.getPossible().contains(value))
                return n;
        }

        return null;
    }

    public static Number[] getColumn(Number[][] block, int y) {
        Number[] column = new Number[9];

        for (int x = 0; x <= 8; x++) {
            column[x] = block[x][y];
        }

        return column;
    }

    public static Number getNumberWithXPossibilities(Number[][] block, int possibilities) {
        for (Number[] row : block) {
            for (Number n : row) {
                if (!n.hasValue() && n.getPossible().size() == possibilities) {
                    return n;
                }
            }
        }
        return null;
    }

    public static boolean isSolvable(Number[][] block) {
        for (Number[] numbers : block) {
            for (Number n : numbers) {
                if (!n.hasValue() && n.getPossible().size() == 0)
                    return false;
            }
        }

        return true;
    }

    public static Number[][] createDeepCopy(Number[][] copyable) {
        Number[][] copy = new Number[9][9];

        for (Number[] row : copyable) {
            for (Number n : row) {
                copy[n.getX()][n.getY()] = n.clone();
            }
        }

        return copy;
    }

    public static void revertToPreviousState(Riddle riddle, Number[][] previousState) {
        for (Number[] row : previousState) {
            for (Number n : row) {
                riddle.set(n.getX(), n.getY(), (byte) n.getValue());
            }
        }
    }

    public static boolean hasXAllFilledIn(Number[][] state, int value) {
        int amount = 0;

        for (Number[] row : state) {
            for (Number n : row) {
                if (n.getValue() == value)
                    amount++;
            }
        }

        return amount == 9;
    }
}
