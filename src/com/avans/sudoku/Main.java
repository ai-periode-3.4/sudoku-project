package com.avans.sudoku;

import com.avans.sudoku.utils.SudokuUtils;
import de.sfuhrm.sudoku.Creator;
import de.sfuhrm.sudoku.GameMatrix;
import de.sfuhrm.sudoku.Riddle;

import java.util.*;

public class Main {

    private static Number[][] numbers = new Number[9][9];

    private static final Stack<Choice> previousStates = new Stack<>();

    public static void main(String[] args) {
        long totalTime = 0;
        int amount = 100;
        long shortest = 0;
        long longest = 0;

        for(int i = 0; i < amount; i++) {
            long time = generateSudoku();

            totalTime += time;

            if(shortest == 0 || time < shortest)
                shortest = time;

            else if(longest == 0 || time > longest)
                longest = time;
        }

        System.out.println();
        System.out.println("Total Sudoku's solved: " + amount);
        System.out.println("Total Time: " + totalTime + "ms");
        System.out.println("Longest Time: " + longest + "ms");
        System.out.println("Shortest Time: " + shortest + "ms");
        System.out.println("Average Time: " + (totalTime / amount) + "ms");
    }

    //TODO: Make it take a pre-made sudoku
    public static long generateSudoku() {
        previousStates.clear();

        GameMatrix matrix = Creator.createFull();
        Riddle riddle = Creator.createRiddle(matrix);

        for (byte[] array : riddle.getArray()) {
            System.out.println(Arrays.toString(array));
        }

        System.out.println();

        for (int x = 0; x <= 8; x++) {
            for (int y = 0; y <= 8; y++) {
                numbers[x][y] = new Number(x, y, riddle.get(x, y), new HashSet<>());
            }
        }


        for (int x = 0; x <= 8; x++) {
            for (int y = 0; y <= 8; y++) {
                Number n = numbers[x][y];

                if (!n.hasValue()) {
                    n.setPossible(SudokuUtils.getPossibleSolutions(riddle, x, y));
                }
            }
        }


        for (int x = 0; x <= 8; x++) {
            for (int y = 0; y <= 8; y++) {
                Number n = numbers[x][y];

                if (!n.hasValue() && n.getPossible().size() == 1)
                    SudokuUtils.update(riddle, numbers, x, y, (int) n.getPossible().toArray()[0]);
            }
        }

        long milliseconds = System.currentTimeMillis();

        solveSudoku(riddle);

        long now = System.currentTimeMillis();

        System.out.println("Time: " + (now - milliseconds) + "ms");

        return (now - milliseconds);
    }

    public static void solveSudoku(Riddle riddle) {
        int previousUpdate = -1;

        while (!SudokuUtils.isFinished(numbers) && previousUpdate != 0) {
            previousUpdate = 0;

            for (int v = 1; v <= 9; v++) {
                if(!SudokuUtils.hasXAllFilledIn(numbers, v)) {
                    for (int blockX = 0; blockX < 3; blockX++) {
                        for (int blockY = 0; blockY < 3; blockY++) {
                            Number[][] spliced = SudokuUtils.splice(numbers, blockX, blockY);

                            int i = SudokuUtils.getNumberCountBlock(spliced, v);

                            if (i == 1) {
                                Number n = SudokuUtils.getNumberWithPossibilityBlock(spliced, v);

                                if (n != null) {
                                    SudokuUtils.update(riddle, numbers, n.getX(), n.getY(), v);
                                    previousUpdate++;
                                }
                            }
                        }
                    }

                    for (int x = 0; x <= 8; x++) {
                        int i = SudokuUtils.getNumberCountRow(numbers[x], v);

                        if (i == 1) {
                            Number n = SudokuUtils.getNumberWithPossibilityRow(numbers[x], v);

                            if (n != null) {
                                SudokuUtils.update(riddle, numbers, n.getX(), n.getY(), v);
                                previousUpdate++;
                            }
                        }
                    }

                    for (int y = 0; y <= 8; y++) {
                        Number[] column = SudokuUtils.getColumn(numbers, y);

                        int i = SudokuUtils.getNumberCountRow(column, v);

                        if (i == 1) {
                            Number n = SudokuUtils.getNumberWithPossibilityRow(column, v);

                            if (n != null) {
                                SudokuUtils.update(riddle, numbers, n.getX(), n.getY(), v);
                                previousUpdate++;
                            }
                        }
                    }
                }
            }
            System.out.println();

            for (byte[] array : riddle.getArray()) {
                System.out.println(Arrays.toString(array));
            }

            if (!SudokuUtils.isSolvable(numbers)) {
                Choice previousChoice = null;

                while (previousChoice == null || (previousStates.size() != 0 && previousChoice.getPreviousChosen().size() == previousChoice.getState()[previousChoice.getNumber().getX()][previousChoice.getNumber().getY()].getPossible().size())) {
                    previousChoice = previousStates.pop();
                }

                makeChoice(riddle, previousChoice);
                break;
            }
        }

        if(!SudokuUtils.isFinished(numbers)) {
            makeChoice(riddle, null);
        }
    }

    public static void makeChoice(Riddle riddle, Choice previousChoice) {
        if (previousChoice == null)

            for (int nop = 2; nop <= 9; nop++) {
                Number n = SudokuUtils.getNumberWithXPossibilities(numbers, nop);

                if (n != null) {
                    int chosenValue = (int) n.possible.toArray()[0];

                    Choice choice = new Choice(SudokuUtils.createDeepCopy(numbers), n.clone(), new HashSet<>(), chosenValue);

                    previousStates.push(choice);

                    System.out.println("Making new choice at X: " + n.getX() + " Y: " + n.getY() + " V:" + chosenValue);

                    SudokuUtils.update(riddle, numbers, n.getX(), n.getY(), chosenValue);
                    break;
                }
            }
        else {
            numbers = previousChoice.getState();
            SudokuUtils.revertToPreviousState(riddle, numbers);

            Number clone = previousChoice.getNumber().clone();

            Set<Integer> previous = new HashSet<>(previousChoice.getPreviousChosen());
            previous.add(previousChoice.getChosenValue());

            previousChoice.getNumber().getPossible().removeAll(previous);

            if(previousChoice.getNumber().getPossible().size() != 0) {
                int chosenValue = (int) previousChoice.getNumber().getPossible().toArray()[0];

                Choice choice = new Choice(SudokuUtils.createDeepCopy(previousChoice.getState()), clone, previous, chosenValue);

                previousStates.push(choice);

                System.out.println("Redoing choice at X: " + clone.getX() + " Y: " + clone.getY() + " V:" + chosenValue);

                SudokuUtils.update(riddle, numbers, clone.getX(), clone.getY(), chosenValue);
            } else {
                Choice p = null;

                while (p == null || (previousStates.size() != 0 && p.getPreviousChosen().size() == p.getState()[p.getNumber().getX()][p.getNumber().getY()].getPossible().size())) {
                    p = previousStates.pop();
                }

                System.out.println("removing choice at X: " + clone.getX() + " Y: " + clone.getY());

                makeChoice(riddle, p);
            }
        }

        solveSudoku(riddle);
    }
}
