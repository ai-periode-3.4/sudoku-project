package com.avans.sudoku;

import java.util.Set;

public class Choice {

    private Number[][] state;

    private Number number;

    private int chosenValue;

    private Set<Integer> previousChosen;

    public Choice(Number[][] state, Number number, Set<Integer> previousChosen, int chosenValue) {
        this.state = state;
        this.number = number;
        this.chosenValue = chosenValue;
        this.previousChosen = previousChosen;
    }

    public Number[][] getState() {
        return state;
    }

    public Number getNumber() {
        return number;
    }

    public int getChosenValue() {
        return chosenValue;
    }

    public Set<Integer> getPreviousChosen() {
        return previousChosen;
    }
}
